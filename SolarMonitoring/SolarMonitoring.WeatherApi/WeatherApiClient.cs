﻿using System;
using System.Globalization;
using System.Net.Http;
using SolarMonitoring.WeatherApi.Model;

namespace SolarMonitoring.WeatherApi
{
	public static class WeatherApiClient
	{
		public static WeatherData GetCurrentValues()
		{
			// Prepare API-Call
			var client = new HttpClient();
			client.BaseAddress = new Uri("http://wetter.htw-berlin.de/phpFunctions/");

			// Get Response
			var response = client.GetStringAsync("holeAktuelleMesswerte.php?mode=csv&data=5&datum=unix").
			                      Result;
			var culture = CultureInfo.CreateSpecificCulture("en-US");

			try
			{
				var rawResponse = response.Split(',');

				double exposureRateOneValue;
				Double.TryParse(rawResponse[0],
				                NumberStyles.Number,
				                culture,
				                out exposureRateOneValue);

				double exposureRateTwoValue;
				Double.TryParse(rawResponse[1],
				                NumberStyles.Number,
				                culture,
				                out exposureRateTwoValue);

				double illuminanceValue;
				Double.TryParse(rawResponse[2],
				                NumberStyles.Number,
				                culture,
				                out illuminanceValue);

				double windSpeedValue;
				Double.TryParse(rawResponse[3],
				                NumberStyles.Number,
				                culture,
				                out windSpeedValue);

				double windDirectionValue;
				Double.TryParse(rawResponse[4],
				                NumberStyles.Number,
				                culture,
				                out windDirectionValue);

				double airTemperatureValue;
				Double.TryParse(rawResponse[5],
				                NumberStyles.Number,
				                culture,
				                out airTemperatureValue);

				double airPressureValue;
				Double.TryParse(rawResponse[6],
				                NumberStyles.Number,
				                culture,
				                out airPressureValue);

				double airHumidityValue;
				Double.TryParse(rawResponse[7],
				                NumberStyles.Number,
				                culture,
				                out airHumidityValue);

				double precipitationValue;
				Double.TryParse(rawResponse[8],
				                NumberStyles.Number,
				                culture,
				                out precipitationValue);

				double unixTimeStamp = Convert.ToDouble(rawResponse[11].Substring(1));
				var timestamp = Helper.UnixTimeStampToDateTime(unixTimeStamp);

				// Create Object and return
				return new WeatherData(timestamp,
				                       exposureRateOneValue,
				                       exposureRateTwoValue,
				                       illuminanceValue,
				                       windSpeedValue,
				                       windDirectionValue,
				                       airTemperatureValue,
				                       airPressureValue,
				                       airHumidityValue,
				                       precipitationValue);
			}
			catch (Exception)
			{
				throw new Exception("The API did not respond correctly!");
			}
		}


		public static SunRise GetSunRiseValues()
		{
			// Prepare API-Call
			var client = new HttpClient();
			client.BaseAddress = new Uri("http://wetter.htw-berlin.de/phpFunctions/");

			// Get Response
			var response = client.GetStringAsync("holeAktuelleMesswerte.php?mode=csv&data=5&datum=unix").
			                      Result;

			try
			{
				// Splitting the whole response
				var rawResponse = response.Split(',');

				// Parsing the Minutes from string
				int sunRisehour = Convert.ToInt16(rawResponse[9].Substring(0,
				                                                           2));
				int sunRiseminute = Convert.ToInt16(rawResponse[9].Substring(3,
				                                                             2));

				var riseTime = new DateTime(DateTime.Now.Year,
				                            DateTime.Now.Month,
				                            DateTime.Now.Day,
				                            sunRisehour,
				                            sunRiseminute,
				                            0);


				// Parsing the Minutes from string
				int sunDownhour = Convert.ToInt16(rawResponse[10].Substring(0,
				                                                            2));
				int sunDownminute = Convert.ToInt16(rawResponse[10].Substring(3,
				                                                              2));

				var downTime = new DateTime(DateTime.Now.Year,
				                            DateTime.Now.Month,
				                            DateTime.Now.Day,
				                            sunDownhour,
				                            sunDownminute,
				                            0);

				// Create Object and return
				var sunrise = new SunRise();
				sunrise.RiseTime = riseTime;
				sunrise.DownTime = downTime;

				return sunrise;
			}
			catch (Exception)
			{
				throw new Exception("The API did not respond correctly!");
			}
		}
	}
}