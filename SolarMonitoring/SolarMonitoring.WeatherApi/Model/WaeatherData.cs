﻿using System;

namespace SolarMonitoring.WeatherApi.Model
{
	public class WeatherData
	{

		public WeatherData()
		{
		}

		// Object which stores different Values of WeatherStations at one Timestamp
		public WeatherData(DateTime timestamp, double exposureRateOneValue, double exposureRateTwoValue, double illuminanceValue, double windSpeedValue, double windDirectionValue, double airTemperatureValue, double airPressureValue, double airHumidityValue, double precipitationValue)
		{
			// ToDo: Set Units!
			// We don't handle different Units yet. 

			TimeStamp = timestamp;

			ExposureRateOne = exposureRateOneValue;
			// Expected Unit is: W / m²

			ExposureRateTwo = exposureRateTwoValue;
			// Expected Unit is: W / m²

			Illuminance = illuminanceValue;
			// Expected Unit is: kLux

			WindSpeed = windSpeedValue;
			// Expected Unit is: m/s

			WindDirection = windDirectionValue;
			// Expected Unit is: degrees

			AirTemperature = airTemperatureValue;
			// Expected Unit is: °C

			AirHumidity = airHumidityValue;
			// Expected Unit is: %

			AirPressure = airPressureValue;
			// Expected Unit is: hPa

			Precipitation = precipitationValue;
			// Expected Unit is: mm
		}

		public int Id { get; set; }

		public double ExposureRateOne { get; set; }

		public double ExposureRateTwo { get; set; }

		public double Illuminance { get; set; }

		public double WindSpeed { get; set; }

		public double WindDirection { get; set; }

		public double AirTemperature { get; set; }

		public double AirHumidity { get; set; }

		public double AirPressure { get; set; }

		public double Precipitation { get; set; }

		public DateTime TimeStamp { get; set; }
	}
}