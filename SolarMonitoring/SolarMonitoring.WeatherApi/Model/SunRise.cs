﻿using System;

namespace SolarMonitoring.WeatherApi.Model
{
	public class SunRise
	{
		public int Id { get; set; }

		public DateTime RiseTime { get; set; }

		public DateTime DownTime { get; set; }

		public bool IsCurrentDay()
		{
			if (RiseTime != DateTime.MinValue && RiseTime.ToShortDateString() == DateTime.Now.ToShortDateString())
			{
				return true;
			}
			return false;
		}
	}
}