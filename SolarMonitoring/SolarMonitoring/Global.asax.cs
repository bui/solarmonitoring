﻿using System.Data.Entity;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SolarMonitoring.App_Start;

namespace SolarMonitoring
{
	public class MvcApplication: HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			Database.SetInitializer(new MonitoringDbInitializer());

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AuthConfig.RegisterAuth();

			JobConfig.StartScheduler();
		}

		protected void Application_End()
		{
			JobConfig.StopScheduler();
		}
	}
}