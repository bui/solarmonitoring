﻿using Quartz;
using SolarMonitoring.Models;
using SolarMonitoring.WeatherApi;

namespace SolarMonitoring.Jobs
{
	public class WeatherStationJob : IJob
	{
		private MonitoringContext db = new MonitoringContext();

		public void Execute(IJobExecutionContext context)
		{
			//ToDo: Log if something goes wrong
			//ToDo: Send E-Mail if something goes wrong
			var weatherData = WeatherApiClient.GetCurrentValues();
			db.WeatherData.Add(weatherData);
			db.SaveChanges();
		}
	}
}