﻿using Quartz;
using SolarMonitoring.Models;
using SolarMonitoring.WeatherApi;

namespace SolarMonitoring.Jobs
{
	public class SunRiseJob: IJob
	{
		private MonitoringContext db = new MonitoringContext();

		public void Execute(IJobExecutionContext context)
		{
			//ToDo: Log if something goes wrong
			//ToDo: Send E-Mail if something goes wrong
			var sunriseData = WeatherApiClient.GetSunRiseValues();
			db.SunRises.Add(sunriseData);
			db.SaveChanges();
		}
	}
}