﻿using System.Data.Entity;
using SolarMonitoring.Migrations;
using SolarMonitoring.Models;

namespace SolarMonitoring.App_Start
{
	public class MonitoringDbInitializer: DropCreateDatabaseIfModelChanges<MonitoringContext>
	{
		protected override void Seed(MonitoringContext context)
		{
			//Real Data used in productionSystems
			DbHelper.CreateSolarModules(context);
		}
	}
}