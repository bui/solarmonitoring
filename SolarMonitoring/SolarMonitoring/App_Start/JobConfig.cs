﻿using Quartz;
using Quartz.Impl;
using SolarMonitoring.Jobs;

namespace SolarMonitoring.App_Start
{
	public static class JobConfig
	{
		static readonly ISchedulerFactory m_SchedFact = new StdSchedulerFactory();

		public static void StartScheduler()
		{
			// construct a scheduler factory
			

			// get a scheduler
			IScheduler sched = m_SchedFact.GetScheduler();
			sched.Start();

			// Job to get minutely Data from the WeatherStationJob
			var weatherStationJob = JobBuilder.Create<WeatherStationJob>().
			                                   WithIdentity("weatherStationJob").
			                                   WithDescription("Get's weatherStation values from HTW-Berlins API").
			                                   Build();

			var minuteTrigger = TriggerBuilder.Create().
			                                   WithCronSchedule("0 0/1 * 1/1 * ? *").
			                                   StartNow().
			                                   Build();

			sched.ScheduleJob(weatherStationJob,
			                  minuteTrigger);

			// Job to get minutely Data from the WeatherStationJob
			var sunRiseJob = JobBuilder.Create<SunRiseJob>().
																				 WithIdentity("sunrise Job").
																				 WithDescription("Get's information about the sunrise from HTW-Berlins API").
																				 Build();

			var dailyTrigger = TriggerBuilder.Create().
																				 WithCronSchedule("0 0 6 1/1 * ? *").
																				 StartNow().
																				 Build();

			sched.ScheduleJob(sunRiseJob,
												dailyTrigger);
		}

		public static void StopScheduler()
		{
			IScheduler sched = m_SchedFact.GetScheduler();
			sched.Shutdown();
		}
	}
}