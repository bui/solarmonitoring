﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Emporer.Unit;
using OpEnMs.EnergyData;
using SolarMonitoring.Models;

namespace SolarMonitoring.Migrations
{
	public static class DbHelper
	{
		public static void CreateSolarModules(MonitoringContext context)
		{
			CreateSources(context);

			// Creating Solar Moduls for HTW-Berlin at each migration
			var danfosSchott = new SolarModule();
			danfosSchott.Name = "Danfoss DLX 2.9 (Schott)";
			danfosSchott.EnergyConsumptions = CreateTestDataEnergyConsumptions(context).ToList();
			context.SolarModules.Add(danfosSchott);

			var danfosAleo = new SolarModule();
			danfosAleo.Name = "Danfoss DLX 2.9 (Aleo s_19)";
			danfosAleo.EnergyConsumptions = CreateTestDataEnergyConsumptions(context).ToList();
			context.SolarModules.Add(danfosAleo);

			var sma3000 = new SolarModule();
			sma3000.Name = "SMA SB3000HF (Schott)";
			sma3000.EnergyConsumptions = CreateTestDataEnergyConsumptions(context).ToList();
			context.SolarModules.Add(sma3000);

			context.SaveChanges();
		}

		private static IEnumerable<EnergyConsumption> CreateTestDataEnergyConsumptions(MonitoringContext context)
		{
			var energyConsumptions = new Collection<EnergyConsumption>();


			foreach (var source in context.Sources)
			{
				var readings = CreateTestDataReadings().
					ToList();
				var energyConsumption = new EnergyConsumption
				{
					Readings = readings,
					Source = source,
					Unit = new Unit()
				};
				energyConsumptions.Add(energyConsumption);
				context.EnergyConsumptions.Add(energyConsumption);
			}

			context.SaveChanges();

			return energyConsumptions;
		}

		private static IEnumerable<Reading> CreateTestDataReadings()
		{
			var rnd = new Random();
			var readings = new Collection<Reading>();

			for (int i = 0; i <= 25; i++)
			{
				var reading = new Reading
				{
					//30 is Max-Value here
					Value = (rnd.NextDouble() * (30 + i)) + i,
					Timestamp = DateTime.Now.AddDays(i)
				};
				readings.Add(reading);
			}

			return readings;
		}

		public static void CreateSources(MonitoringContext context)
		{
			//ToDo: Add here different types of data we want to measure AC / DC ..
			var electicity = new Source
			{
				Name = "Electricity"
			};
			
			context.Sources.Add(electicity);
			context.SaveChanges();
		}
	}
}