﻿using System.Linq;
using System.Web.Mvc;
using OpEnMs.EnergyChart.Factories;
using SolarMonitoring.Models;
using SolarMonitoring.ViewModels;

namespace SolarMonitoring.Controllers
{
	public class HomeController: Controller
	{
		private MonitoringContext db = new MonitoringContext();
		private readonly HighchartFactory m_HighchartFactory;

		public HomeController()
		{
			m_HighchartFactory = new HighchartFactory();
		}

		public ActionResult Index()
		{
			var viewModel = new HomeViewModel();

			//viewModel.LineChart = m_HighchartFactory.CreateLineChart(db.SolarModules.FirstOrDefault());

			return View(db.Readings.ToList());
			
		}


	}
}
