﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using OpEnMs.EnergyData;
using SolarMonitoring.Models;

namespace SolarMonitoring.Controllers
{
    public class ReadingController : Controller
    {
        private MonitoringContext db = new MonitoringContext();

        //
        // GET: /Reading/

        public ActionResult Index()
        {
            return View(db.Readings.ToList());
        }

        //
        // GET: /Reading/Details/5

        public ActionResult Details(int id = 0)
        {
            Reading reading = db.Readings.Find(id);
            if (reading == null)
            {
                return HttpNotFound();
            }
            return View(reading);
        }

        //
        // GET: /Reading/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Reading/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Reading reading)
        {
            if (ModelState.IsValid)
            {
                db.Readings.Add(reading);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reading);
        }

        //
        // GET: /Reading/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Reading reading = db.Readings.Find(id);
            if (reading == null)
            {
                return HttpNotFound();
            }
            return View(reading);
        }

        //
        // POST: /Reading/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Reading reading)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reading).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reading);
        }

        //
        // GET: /Reading/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Reading reading = db.Readings.Find(id);
            if (reading == null)
            {
                return HttpNotFound();
            }
            return View(reading);
        }

        //
        // POST: /Reading/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reading reading = db.Readings.Find(id);
            db.Readings.Remove(reading);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}