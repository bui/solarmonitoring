﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Objects;
using System.Linq;
using System.Web.Mvc;
using OpEnMs.EnergyChart.Factories;
using OpEnMs.EnergyData;
using SolarMonitoring.Models;
using SolarMonitoring.ViewModels;
using SolarMonitoring.ViewModels.WeatherStation;
using SolarMonitoring.WeatherApi.Model;

namespace SolarMonitoring.Controllers
{
	public class WeatherStationController: Controller
	{
		private MonitoringContext db = new MonitoringContext();
		private readonly HighchartFactory m_HighchartFactory;

		public WeatherStationController()
		{
			m_HighchartFactory = new HighchartFactory();
		}

		//
		// GET: /WeatherStation/AirTemperature
		public ActionResult Index()
		{
			var viewModel = new WeatherDataViewModel();
			var date = DateTime.Today;

			viewModel.WeatherData = db.WeatherData.Where(wd => EntityFunctions.TruncateTime(wd.TimeStamp) >= date).ToList().Last();

			return View(viewModel);
		}

		//
		// GET: /WeatherStation/AirTemperature
		public ActionResult AirTemperature()
		{
			// Filter for todays data
			var weatherData = GetTodaysData();

			if (weatherData.Count() == 0)
			{
				return HttpNotFound();
			}
			var viewModel = new AirTemperatureViewModel();
			viewModel.AirTemperatures = new Collection<Reading>();

			foreach (var data in weatherData)
			{
				viewModel.AirTemperatures.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.AirTemperature,
				                                                                                  data.TimeStamp));
			}
			viewModel.Date = DateTime.Today;

			viewModel.HighChart = m_HighchartFactory.CreateLineChart(viewModel.AirTemperatures,
			                                   viewModel.Name,
			                                   viewModel.Unit);

			return View(viewModel);
		}

		//
		// GET: /WeatherStation/AirPressure
		public ActionResult AirPressure()
		{
			// Filter for todays data
			var weatherData = GetTodaysData();

			if (weatherData.Count() == 0)
			{
				return HttpNotFound();
			}
			var viewModel = new AirPressureViewModel();
			viewModel.AirPressures = new Collection<Reading>();

			foreach (var data in weatherData)
			{
				viewModel.AirPressures.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.AirPressure,
																																													data.TimeStamp));
			}
			viewModel.Date = DateTime.Today;

			viewModel.HighChart = m_HighchartFactory.CreateLineChart(viewModel.AirPressures,
																				 viewModel.Name,
																				 viewModel.Unit);

			return View(viewModel);
		}

		//
		// GET: /WeatherStation/AirHumidity
		public ActionResult AirHumidity()
		{

			// Filter for todays data
			var weatherData = GetTodaysData();

			if (weatherData.Count() == 0)
			{
				return HttpNotFound();
			}
			var viewModel = new AirHumidityViewModel();
			viewModel.AirHumidity = new Collection<Reading>();

			foreach (var data in weatherData)
			{
				viewModel.AirHumidity.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.AirHumidity,
																																													data.TimeStamp));
			}
			viewModel.Date = DateTime.Today;

			viewModel.HighChart = m_HighchartFactory.CreateLineChart(viewModel.AirHumidity,
																				 viewModel.Name,
																				 viewModel.Unit);

			return View(viewModel);
		}

		//
		// GET: /WeatherStation/Precipitation
		public ActionResult Precipitation()
		{

			// Filter for todays data
			var weatherData = GetTodaysData();

			if (weatherData.Count() == 0)
			{
				return HttpNotFound();
			}
			var viewModel = new PrecipitationViewModel();
			viewModel.Precipitation = new Collection<Reading>();

			foreach (var data in weatherData)
			{
				viewModel.Precipitation.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.Precipitation,
																																													data.TimeStamp));
			}
			viewModel.Date = DateTime.Today;

			viewModel.HighChart = m_HighchartFactory.CreateLineChart(viewModel.Precipitation,
																				 viewModel.Name,
																				 viewModel.Unit);

			return View(viewModel);
		}

		//
		// GET: /WeatherStation/WindSpeed
		public ActionResult WindSpeed()
		{

			// Filter for todays data
			var weatherData = GetTodaysData();

			if (weatherData.Count() == 0)
			{
				return HttpNotFound();
			}
			var viewModel = new WindSpeedViewModel();
			viewModel.WindSpeed = new Collection<Reading>();

			foreach (var data in weatherData)
			{
				viewModel.WindSpeed.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.WindSpeed,
																																													data.TimeStamp));
			}
			viewModel.Date = DateTime.Today;

			viewModel.HighChart = m_HighchartFactory.CreateLineChart(viewModel.WindSpeed,
																				 viewModel.Name,
																				 viewModel.Unit);

			return View(viewModel);
		}

		//
		// GET: /WeatherStation/Illuminance
		public ActionResult Illuminance()
		{

			// Filter for todays data
			var weatherData = GetTodaysData();

			if (weatherData.Count() == 0)
			{
				return HttpNotFound();
			}
			var viewModel = new IlluminanceViewModel();
			viewModel.Illuminance = new Collection<Reading>();

			foreach (var data in weatherData)
			{
				viewModel.Illuminance.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.Illuminance,
																																													data.TimeStamp));
			}
			viewModel.Date = DateTime.Today;

			viewModel.HighChart = m_HighchartFactory.CreateLineChart(viewModel.Illuminance,
																				 viewModel.Name,
																				 viewModel.Unit);

			return View(viewModel);
		}

		//
		// GET: /WeatherStation/ExposureRate
		public ActionResult ExposureRate()
		{

			// Filter for todays data
			var weatherData = GetTodaysData();

			if (weatherData.Count() == 0)
			{
				return HttpNotFound();
			}
			var viewModel = new ExposureRateViewModel();
			viewModel.ExposureRateOne = new Collection<Reading>();
			viewModel.ExposureRateTwo = new Collection<Reading>();

			foreach (var data in weatherData)
			{
				viewModel.ExposureRateOne.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.ExposureRateOne,
																																													data.TimeStamp));
				viewModel.ExposureRateTwo.Add(ViewModelHelper.CreateReadingFromWeatherStationData(data.ExposureRateTwo,
																																													data.TimeStamp));
			}
			viewModel.Date = DateTime.Today;

			var seriesArray = new IEnumerable<Reading>[2];
			seriesArray[0] = viewModel.ExposureRateOne;
			seriesArray[1] = viewModel.ExposureRateTwo;

			viewModel.HighChart = m_HighchartFactory.CreateLineChart(seriesArray,
																				 viewModel.Name,
																				 viewModel.Unit);

			return View(viewModel);
		}


		private IEnumerable<WeatherData> GetTodaysData()
		{
			var date = DateTime.Today;
			return db.WeatherData.Where(wd => EntityFunctions.TruncateTime(wd.TimeStamp) >= date).OrderBy(wd => wd.TimeStamp);
		}
	}
}