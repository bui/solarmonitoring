﻿using System.Linq;
using System.Web.Mvc;
using SolarMonitoring.Models;

namespace SolarMonitoring.Controllers
{
	public class SolarModuleController : Controller
	{
		private MonitoringContext db = new MonitoringContext();

		//
		// GET: /SolarModule/

		public ActionResult Index()
		{
			return View(db.SolarModules.ToList());
		}


		//
		// GET: /SolarModule/Details/5
		public ActionResult Details(int id = 0)
		{
			SolarModule solarModule = db.SolarModules.Find(id);
			if (solarModule == null)
			{
				return HttpNotFound();
			}
			return View(solarModule);
		}

		//
		// GET: /SolarModule/Create

		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /SolarModule/Create

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(SolarModule solarModule)
		{
			if (ModelState.IsValid)
			{
				db.SolarModules.Add(solarModule);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			return View(solarModule);
		}

	}
}