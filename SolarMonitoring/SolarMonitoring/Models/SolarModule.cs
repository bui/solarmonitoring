﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OpEnMs.EnergyChart;
using OpEnMs.EnergyData;

namespace SolarMonitoring.Models
{
	public class SolarModule : IChartable
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }
		public ICollection<EnergyConsumption> EnergyConsumptions { get; set; }
	}
}