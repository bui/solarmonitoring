﻿using System.Data.Entity;
using Emporer.Unit;
using OpEnMs.EnergyData;
using SolarMonitoring.WeatherApi.Model;

namespace SolarMonitoring.Models
{
	public class MonitoringContext: DbContext,
																IMonitoringContext
	{
		public DbSet<Unit> Units
		{
			get;
			set;
		}
		public DbSet<EnergyConsumption> EnergyConsumptions
		{
			get;
			set;
		}
		public DbSet<Reading> Readings
		{
			get;
			set;
		}
		public DbSet<SolarModule> SolarModules
		{
			get;
			set;
		}
		public DbSet<Source> Sources
		{
			get;
			set;
		}
		public DbSet<UserProfile> UserProfiles
		{
			get;
			set;
		}
		public DbSet<WeatherData> WeatherData
		{
			get;
			set;
		}
		public DbSet<SunRise> SunRises
		{
			get;
			set;
		}
	}

	public interface IMonitoringContext
	{
		DbSet<EnergyConsumption> EnergyConsumptions
		{
			get;
			set;
		}
		DbSet<Reading> Readings
		{
			get;
			set;
		}
		DbSet<Unit> Units
		{
			get;
			set;
		}
		DbSet<UserProfile> UserProfiles
		{
			get;
			set;
		}
		DbSet<SolarModule> SolarModules
		{
			get;
			set;
		}
		DbSet<WeatherData> WeatherData
		{
			get;
			set;
		}
		DbSet<SunRise> SunRises
		{
			get;
			set;
		}
	}
}