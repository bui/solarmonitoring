﻿using System;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels
{
	public static class ViewModelHelper
	{
		public static Reading CreateReadingFromWeatherStationData(double value, DateTime dateTime)
		{
			return new Reading() {Timestamp =  dateTime, Value = value};
		}
	}
}