﻿using System;
using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class AirTemperatureViewModel
	{
		public ICollection<Reading> AirTemperatures { get; set; }

		public DateTime Date { get; set; }

		public String Name
		{
			get
			{
				return string.Format("AirTemperature");
			}
		}

		public String Unit
		{
			get
			{
				return "°C";
			}
		}

		public Highcharts HighChart { get; set; }
	}
}