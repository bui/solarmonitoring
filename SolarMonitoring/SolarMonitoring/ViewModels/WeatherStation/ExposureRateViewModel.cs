﻿using System;
using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class ExposureRateViewModel
	{
		public ICollection<Reading> ExposureRateOne { get; set; }
		public ICollection<Reading> ExposureRateTwo
		{
			get;
			set;
		}

		public DateTime Date { get; set; }

		public String Name
		{
			get
			{
				return string.Format("ExposureRate");
			}
		}

		public String Unit
		{
			get
			{
				return "W/m²";
			}
		}

		public Highcharts HighChart { get; set; }
	}
}