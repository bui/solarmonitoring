﻿using System;
using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class IlluminanceViewModel
	{
		public ICollection<Reading> Illuminance
		{
			get;
			set;
		}

		public DateTime Date { get; set; }

		public String Name
		{
			get
			{
				return string.Format("Illuminance");
			}
		}

		public String Unit
		{
			get
			{
				return "kLux";
			}
		}

		public Highcharts HighChart { get; set; }
	}
}