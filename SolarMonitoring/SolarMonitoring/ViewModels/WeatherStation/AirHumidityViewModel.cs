﻿using System;
using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class AirHumidityViewModel
	{
		public ICollection<Reading> AirHumidity { get; set; }

		public DateTime Date { get; set; }

		public String Name
		{
			get
			{
				return string.Format("AirHumidity");
			}
		}

		public String Unit
		{
			get
			{
				return "%";
			}
		}

		public Highcharts HighChart { get; set; }
	}
}