﻿using System;
using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class PrecipitationViewModel
	{
		public ICollection<Reading> Precipitation
		{
			get;
			set;
		}

		public DateTime Date { get; set; }

		public String Name
		{
			get
			{
				return string.Format("Precipitation");
			}
		}

		public String Unit
		{
			get
			{
				return "mm";
			}
		}

		public Highcharts HighChart { get; set; }
	}
}