﻿using System;
using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class WindSpeedViewModel
	{
		public ICollection<Reading> WindSpeed
		{
			get;
			set;
		}

		public DateTime Date { get; set; }

		public String Name
		{
			get
			{
				return string.Format("WindSpeed");
			}
		}

		public String Unit
		{
			get
			{
				return "m/s";
			}
		}

		public Highcharts HighChart { get; set; }
	}
}