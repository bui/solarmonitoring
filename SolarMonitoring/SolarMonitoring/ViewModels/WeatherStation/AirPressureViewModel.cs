﻿using System;
using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.EnergyData;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class AirPressureViewModel
	{
		public ICollection<Reading> AirPressures { get; set; }

		public DateTime Date { get; set; }

		public String Name
		{
			get
			{
				return string.Format("AirPressure");
			}
		}

		public String Unit
		{
			get
			{
				return "hPa";
			}
		}

		public Highcharts HighChart { get; set; }
	}
}