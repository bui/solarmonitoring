﻿using SolarMonitoring.WeatherApi.Model;

namespace SolarMonitoring.ViewModels.WeatherStation
{
	public class WeatherDataViewModel
	{
		public WeatherData WeatherData
		{
			get; set; }
	}
}